# Catalog.API

### Простой API микросервис каталога продуктов c использованием MongoDb.

### Стек:
1. .Net 7 / AspNetCore
2. MongoDb
3. Docker

Сервис для личного ознакомления с docker-compose, микросервисной архитектурой.
Источник: https://medium.com/aspnetrun/microservices-architecture-on-net-3b4865eea03f
 
Запускалось в среде WSL2/Ubuntu + Docker desktop.

Запуск: _`docker-compose -f docker-compose.yml -f docker-compose.development.yml up`_