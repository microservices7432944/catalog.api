﻿using Catalog.API.Data.Interfaces;
using Catalog.API.Entities;
using Catalog.API.Settings;
using MongoDB.Driver;

namespace Catalog.API.Data;

/// <inheritdoc/>
public class CatalogContext : ICatalogContext
{
    public CatalogContext(IConfiguration configuration)
    {
        var dbConfig = configuration.GetSection(nameof(DatabaseSettings));
        
        var client = new MongoClient(dbConfig["ConnectionString"]);
        var database = client.GetDatabase(dbConfig["DatabaseName"]);

        Products = database.GetCollection<Product>(dbConfig["CollectionName"]);
    }
    
    public IMongoCollection<Product> Products { get; }
}