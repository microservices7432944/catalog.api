﻿using Catalog.API.Entities;
using MongoDB.Driver;

namespace Catalog.API.Data.Interfaces;

/// <summary>
/// Контекст продукта
/// </summary>
public interface ICatalogContext
{
    /// <summary>
    /// Продукты
    /// </summary>
    IMongoCollection<Product> Products { get; }
}