﻿namespace Catalog.API.Settings;

/// <summary>
/// Настройки подключения к БД
/// </summary>
public class DatabaseSettings
{
    /// <summary>
    /// Строка подключения
    /// </summary>
    public required string ConnectionString { get; set; }
    
    /// <summary>
    /// Имя БД
    /// </summary>
    public required string DatabaseName { get; set; }
    
    /// <summary>
    /// Наименование коллекции
    /// </summary>
    public required string CollectionName { get; set; }
}