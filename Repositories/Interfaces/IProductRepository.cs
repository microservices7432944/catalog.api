﻿using Catalog.API.Entities;

namespace Catalog.API.Repositories.Interfaces;

/// <summary>
/// Репозиторий продуктов
/// </summary>
public interface IProductRepository
{
    /// <summary>
    /// Получить продукты
    /// </summary>
    Task<IEnumerable<Product>> GetProducts();
    
    /// <summary>
    /// Получить продукт
    /// </summary>
    /// <param name="id">Идентификатор</param>
    Task<Product?> GetProduct(string id);
    
    /// <summary>
    /// Получить продукт по наименованию
    /// </summary>
    /// <param name="name">Наименование</param>
    Task<IEnumerable<Product>> GetProductByName(string name);
    
    /// <summary>
    /// Получить продукты по категории
    /// </summary>
    /// <param name="categoryName">Наименование категории</param>
    Task<IEnumerable<Product>> GetProductByCategory(string categoryName);
    
    /// <summary>
    /// Создать продукт
    /// </summary>
    /// <param name="product">Продукт</param>
    Task CreateProduct(Product product);
    
    /// <summary>
    /// Обновить продукт
    /// </summary>
    /// <param name="product">Продукт</param>
    Task<bool> UpdateProduct(Product product);
    
    /// <summary>
    /// Удалить продукт
    /// </summary>
    /// <param name="id">Идентификатор</param>
    Task<bool> DeleteProduct(string id);
}