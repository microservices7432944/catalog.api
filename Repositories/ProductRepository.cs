﻿using Catalog.API.Data.Interfaces;
using Catalog.API.Entities;
using Catalog.API.Repositories.Interfaces;
using MongoDB.Driver;

namespace Catalog.API.Repositories;

/// <inheritdoc/>
public class ProductRepository : IProductRepository
{
    private readonly ICatalogContext _catalogContext;

    public ProductRepository(ICatalogContext catalogContext)
    {
        _catalogContext = catalogContext ?? throw new ArgumentNullException(nameof(catalogContext));
    }

    public async Task<IEnumerable<Product>> GetProducts()
    {
        return await _catalogContext
            .Products
            .Find(x => true)
            .ToListAsync();
    }

    public async Task<Product?> GetProduct(string id)
    {
        return await _catalogContext
            .Products
            .Find(x => x.Id == id)
            .FirstOrDefaultAsync();
    }

    public async Task<IEnumerable<Product>> GetProductByName(string name)
    {
        var filter = Builders<Product>.Filter.ElemMatch(p => p.Name, name);
        
        return await _catalogContext
            .Products
            .Find(filter)
            .ToListAsync();
    }

    public async Task<IEnumerable<Product>> GetProductByCategory(string categoryName)
    {
        var filter = Builders<Product>.Filter.Eq(p => p.Category, categoryName);
        
        return await _catalogContext
            .Products
            .Find(filter)
            .ToListAsync();
    }

    public Task CreateProduct(Product product) 
        => _catalogContext.Products.InsertOneAsync(product);

    public async Task<bool> UpdateProduct(Product product)
    {
        var updateResult = await _catalogContext
            .Products
            .ReplaceOneAsync(filter: x => x.Id == product.Id, replacement: product);
        
        return updateResult.IsAcknowledged && updateResult.ModifiedCount > 0;
    }

    public async Task<bool> DeleteProduct(string id)
    {
        var filter = Builders<Product>.Filter.Eq(p => p.Id, id);
        
        var deleteResult = await _catalogContext
            .Products
            .DeleteOneAsync(filter);
        
        return deleteResult.IsAcknowledged && deleteResult.DeletedCount > 0;
    }
}